# Delight Test

Test realized for Delight using Elixir.

The instructions are located [here](https://adventofcode.com/2018/day/1) (Part 1 and 2).

See the **Results** section below to see the results obtained for the test.

The code made for the parts is available [here](lib/time_device.ex).

## Installation

Make sure you have the correct version of [Erlang](https://www.erlang-solutions.com/resources/download.html), Elixir and associated tools.

- **Erlang version : 22.1**
- **Elixir version : 1.10.1**

To compile the necessary to use the project, execute the following commands in your shell (inside *delight-dest/* directory):

```shell
$> mix
```

## Usage

Data needed by the project are located under the *data/* directory.

> input.txt is the file downloaded from the instructions website.

### Part 1

```shell
$> iex.bat -S mix
iex(1)> TimeDevice.calibrate_part_one("data/input.txt")
```
> *.bat extension is only mandatory on Windows.

### Part 2
```shell
$> iex.bat -S mix
iex(1)> TimeDevice.calibrate_part_two("data/input.txt")
```

## Tests

Some unit tests are also available, they test the examples mentionned in the instructions.

To launch the tests:
```shell
$> mix test
```

## Results

After execution of the different parts results are :
### Part 1 : **474**
### Part 2 : **137041**
