defmodule TimeDeviceTest do
  use ExUnit.Case
  doctest TimeDevice

  test "Time Device calibration : Part 1 Test 1" do
    assert TimeDevice.calibrate_part_one("data/test_p1_1.txt") == 3
  end

  test "Time Device calibration : Part 1 Test 2" do
    assert TimeDevice.calibrate_part_one("data/test_p1_2.txt") == 0
  end

  test "Time Device calibration : Part 1 Test 3" do
    assert TimeDevice.calibrate_part_one("data/test_p1_3.txt") == -6
  end


  test "Time Device calibration : Part 2 Test 1" do
    assert TimeDevice.calibrate_part_two("data/test_p2_1.txt") == 0
  end

  test "Time Device calibration : Part 2 Test 2" do
    assert TimeDevice.calibrate_part_two("data/test_p2_2.txt") == 10
  end

  test "Time Device calibration : Part 2 Test 3" do
    assert TimeDevice.calibrate_part_two("data/test_p2_3.txt") == 5
  end

  test "Time Device calibration : Part 2 Test 4" do
    assert TimeDevice.calibrate_part_two("data/test_p2_4.txt") == 14
  end

end
