defmodule TimeDevice do
  @moduledoc """
    TimeDevice class for Delight test.
  """
  def calibrate_part_one(input) do
    contents = File.read!(input)
    contents
    |> String.split("\n", trim: true)
    |> Enum.map(fn x -> {val, _} = Integer.parse(x); val end)
    |> Enum.sum
  end

  def calibrate_part_two(input) do
    contents = File.read!(input)
    frequencies = contents
    |> String.split("\n", trim: true)
    |> Enum.map(fn x -> {val, _} = Integer.parse(x); val end)
    prev_freq = [0]
    compute(frequencies, 0, prev_freq)
  end

  defp compute(frequencies, index, prev_freq) do
    res = Enum.at(frequencies, index) + Enum.at(prev_freq, 0)
    if Enum.member?(prev_freq, res) == true do
      res
    else
      prev_freq = [res | prev_freq]
      index = index + 1
      if index == length(frequencies) do
        compute(frequencies, 0, prev_freq)
      else
        compute(frequencies, index, prev_freq)
      end
    end
  end
end
